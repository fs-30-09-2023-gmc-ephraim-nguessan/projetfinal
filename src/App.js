import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Composants/Home';
import Articles from './Composants/Articles';

function App() {
  return (
    <div className="App">
      <Home></Home>
    </div>
  );
}

export default App;
