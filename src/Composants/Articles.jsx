import React, { useState } from 'react';

const Article = ({ id, title, description, price, onAddToCart }) => {
  return (
    <div style={{ display: "flex", alignItems: "center", marginBottom: "20px" }}>
      <div style={{ marginRight: "20px" }}>
        
        <img src={process.env.PUBLIC_URL + `/img/${id}.jpg`} alt={title} className='articles'/>
      </div>
      <div>
        
        <h2>{title}</h2>
        <p>{description}</p>
        <p>Prix: ${price}</p>
        
        <button onClick={() => onAddToCart({ id, title, price })}>Ajouter au panier</button>
      </div>
    </div>
  );
};

const Articles = () => {
  // État pour stocker les articles ajoutés au panier
  const [panier, setPanier] = useState([]);

  // Fonction pour ajouter un article au panier
  const ajouterAuPanier = (article) => {
    setPanier([...panier, article]);
  };

  // Fonction pour supprimer un article du panier
  const supprimerDuPanier = (index) => {
    const newPanier = [...panier];
    newPanier.splice(index, 1);
    setPanier(newPanier);
  };

  // Calculer le montant total du panier
  const total = panier.reduce((acc, article) => acc + article.price, 0);

  const handleReturnHome = () => {
    // Redirection vers la page d'accueil
    window.location.href = '/';
  };

  return (
    <div>
      <h1>Page des articles</h1>
      {panier.length > 0 && (
        <div>
          <h2>Articles dans le panier :</h2>
          <ul>
            {panier.map((article, index) => (
              <li key={index}>
                
                {article.title} - ${article.price}

                <button onClick={() => supprimerDuPanier(index)}>Supprimer</button>
              </li>
            ))}
          </ul>
          <p>Total: ${total}</p>
        </div>
      )}
      <Article
        id="casquette1"
        title="Casquette 1"
        description="Description de la casquette 1"
        price={10}
        onAddToCart={ajouterAuPanier}
      />
       <Article
        id="casquette2"
        title="Casquette 2"
        description="Description de la casquette 2"
        price={15}
        onAddToCart={ajouterAuPanier}
      />
      <Article
        id="casquette3"
        title="Casquette 3"
        description="Description de la casquette 3"
        price={20}
        onAddToCart={ajouterAuPanier}
      />
       <Article
        id="casquette4"
        title="Casquette 4"
        description="Description de la casquette 2"
        price={15}
        onAddToCart={ajouterAuPanier}
      />
   <button onClick={handleReturnHome}>Retour à l'accueil</button>
    </div>
  );
};

export default Articles;
