import React, { useState } from "react";
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import image1 from './image1.jpg'; 
import casquettegrand from './casquettegrand.png';
import './Home.css'; 
import Carousel from 'react-bootstrap/Carousel';
import Articles from './Articles'; 

function Home() {
  const [index, setIndex] = useState(0);
  const [showArticles, setShowArticles] = useState(false);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  const redirectToArticleDetails = () => {
    setShowArticles(true);
  };

  return (
    <div>
      {!showArticles && (
        <><div>
          <Navbar expand="lg" className="bg-body-tertiary">
            <Container>
              <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Nav.Link href="#home">Home</Nav.Link>
                  <Nav.Link href="#link">Link</Nav.Link>
                  <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                  </NavDropdown>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
          <div className="image-container">
            <img src={image1} alt="Description de l'image" className="center-image" />
          </div>
          <div style={{ backgroundColor: '#FF6F61', color: 'white', textAlign: 'center', padding: '20px' }}>
            <h1 style={{ fontWeight: 'bold' }}>BIENVENUE.</h1>
            <p style={{ fontWeight: 'bold' }}>Seager a été fondée en 2015 dans le but de perpétuer le courage et la robustesse de l'ancien Ouest à travers la communauté, les expériences et les équipements et vêtements d'extérieur intemporels.</p>
            <p style={{ fontWeight: 'bold' }}>Nous sommes heureux que vous soyez ici, et nous espérons que vous resterez un moment. Nous ne partons nulle part.</p>
          </div>


          <div className="mt-5">
            <Carousel className="image-container" activeIndex={index} onSelect={handleSelect}>
              <Carousel.Item>
                <img
                  className="carousel-container"
                  src={casquettegrand} 
                  alt="First slide" />
                <Carousel.Caption>
                  <h3>First slide label</h3>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img src={process.env.PUBLIC_URL + '/img/chapeau.jpg'} alt="Category Image" className="carousel-container" />
                <Carousel.Caption>
                  <h3>Second slide label</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img src={process.env.PUBLIC_URL + '/img/surf.jpg'} alt="Category Image" className="carousel-container" />
                <Carousel.Caption>
                  <h3>Third slide label</h3>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </div>

          <div>
            <div className="recent-product product">
              <div className="container-fluid">
                <div className="section-header">
                  <h1 style={{ backgroundColor: "lightgrey" }}>ARTICLES DISPONIBLES</h1>
                </div>
                <div className="row align-items-center product-slider product-slider-4">
                  {/* Répétez le bloc cinq fois */}
                  {[...Array(4)].map((_, index) => (
                    <div key={index} className="col-lg-3">
                      <div className="product-item">
                        <div className="product-title">
                          <a href="#">Product Name</a>
                          <div className="ratting">
                            <i className="fa fa-star"></i>
                            <i className="fa fa-star"></i>
                            <i className="fa fa-star"></i>
                            <i className="fa fa-star"></i>
                          </div>
                        </div>
                        <div className="product-image">
                          <a href="#">
                            <img src={process.env.PUBLIC_URL + `/img/casquette${index + 1}.jpg`} alt={`CASQUETTE ${index + 1}`} />
                          </a>
                          <div className="product-action">
                            <a href="#"><i className="fa fa-cart-plus"></i></a>
                            <a href="#"><i className="fa fa-heart"></i></a>
                            <a href="#"><i className="fa fa-search"></i></a>
                          </div>
                        </div>
                        <div className="product-price">
                          <h3><span>$</span>99</h3>
                          <button className="btn" onClick={redirectToArticleDetails}><i className="fa fa-shopping-cart"></i>Buy Now</button>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
          <div className="mt-4">
            <div className="category" >
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-3">
                    <div className="category-item ch-400">
                      <img src={process.env.PUBLIC_URL + '/img/casquette5.jpg'} alt="Category Image" />
                      <a className="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                      </a>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="category-item ch-250">
                      <img src={process.env.PUBLIC_URL + '/img/casquette6.jpg'} alt="Category Image" />
                      <a className="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                      </a>
                    </div>
                    <div className="category-item ch-150">
                      <img src={process.env.PUBLIC_URL + '/img/casquette7.jpg'} alt="Category Image" />
                      <a className="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                      </a>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="category-item ch-150">
                      <img src={process.env.PUBLIC_URL + '/img/culotte.jpg'} alt="Category Image" />
                      <a className="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            
            <div className="section-header">
              <h1 style={{ backgroundColor: "lightgrey", color: "#FF6F61" }}>ARTICLES INDISPONIBLES</h1>
            </div>

            <div className="block5">
              <div className="container-fluid mx-auto">
                <div className="row justify-content-center">
                  
                  <div className="col-md-6" style={{ width: '500px', height: '200px' }}>
                    <Carousel interval={3000} pause={false}>
                      <Carousel.Item>
                        <img
                          className="dblock-w100"
                          src={process.env.PUBLIC_URL + '/img/lunette.jpg'}
                          alt="First slide" />
                      </Carousel.Item>
                      <Carousel.Item>
                        <img
                          className="dblock-w100"
                          src={process.env.PUBLIC_URL + '/img/cactus.jpg'}
                          alt="Second slide" />
                      </Carousel.Item>
                    </Carousel>
                  </div>

                  {/* Overlay blocks */}
                  <div className="col-md-3" style={{ width: '500px', height: '500px', position: 'relative' }}>
                    <div className="overlay-block" style={{ position: 'absolute', top: '0', left: '0', width: '100%', height: '250px' }}>
                      <img className="dblock-w101"
                        src={process.env.PUBLIC_URL + '/img/gas_stack.jpg'}
                        alt="Overlay Image 1"
                        style={{ width: '100%', height: '100%' }} />
                    </div>
                    <div className="overlay-block" style={{ position: 'absolute', bottom: '0', left: '0', width: '100%', height: '250px' }}>
                      <img className="dblock-w101"
                        src={process.env.PUBLIC_URL + '/img/chapeau.jpg'}
                        alt="Overlay Image 2"
                        style={{ width: '100%', height: '100%' }} />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="mt-5">
              <div className="block-container">
                {/* Premier bloc */}
                <div className="block">
                  <img src={process.env.PUBLIC_URL + '/image/icone1.png'} alt="Image 1" className="block-image" />
                  <p className="block-text">Texte du premier bloc</p>
                </div>

                {/* Deuxième bloc */}
                <div className="block">
                  <img src={process.env.PUBLIC_URL + '/image/icone2.png'} alt="Image 2" className="block-image" />
                  <p className="block-text">Texte du deuxième bloc</p>
                </div>

                {/* Troisième bloc */}
                <div className="block">
                  <img src={process.env.PUBLIC_URL + '/image/icone3.png'} alt="Image 3" className="block-image" />
                  <p className="block-text">Texte du troisième bloc</p>
                </div>

                {/* Quatrième bloc */}
                <div className="block">
                  <img src={process.env.PUBLIC_URL + '/image/icone4.png'} alt="Image 4" className="block-image" />
                  <p className="block-text">Texte du quatrième bloc</p>
                </div>
              </div>
            </div>

            <div className="container-fluid">
              <div className="row">
                {/* Premier bloc */}
                <div className="col-md-6">
                  <img src={process.env.PUBLIC_URL + '/img/logo.jpg'} alt="Image 1" />
                </div>

                {/* Deuxième bloc */}
                <div className="col-md-6">
                  <img src={process.env.PUBLIC_URL + '/img/casque.jpg'} alt="Image 1" />
                </div>
              </div>
            </div>

            <div className="mt-5">
              <div className="footer" style={{ backgroundColor: "black", color: "pink", marginTop: "50px" }}>
                <div className="container-fluid">
                  <div className="row">
                    <div className="col-lg-3 col-md-6">
                      <div className="footer-widget">
                        <h2>Get in Touch</h2>
                        <div className="contact-info">
                          <p><i className="fa fa-map-marker"></i>123 E Store, Grd Lahou, CIV</p>
                          <p><i className="fa fa-envelope"></i>email@example.com</p>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-3 col-md-6">
                      <div className="footer-widget">
                        <h2>Follow Us</h2>
                        <div className="contact-info">
                          <div className="social">
                            <a href=""><i className="fab fa-twitter"></i></a>
                            <a href=""><i className="fab fa-facebook-f"></i></a>
                            <a href=""><i className="fab fa-linkedin-in"></i></a>
                            <a href=""><i className="fab fa-instagram"></i></a>
                            <a href=""><i className="fab fa-youtube"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-3 col-md-6">
                      <div className="footer-widget">
                        <h2>Company Info</h2>
                        <ul>
                          <li><a href="#">A Propos de nous</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                          <li><a href="#">Terms & Condition</a></li>
                        </ul>
                      </div>
                    </div>

                    <div className="col-lg-3 col-md-6">
                      <div className="footer-widget">
                        <h2>Purchase Info</h2>
                        <ul>
                          <li><a href="#">Pyament Policy</a></li>
                          <li><a href="#">Shipping Policy</a></li>
                          <li><a href="#">Return Policy</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </>
      )}

      {showArticles && <Articles></Articles>}
    </div>
  );
}

export default Home;
